import "babel-polyfill"
import $ from 'jquery'
import whatInput from 'what-input'
import stickyNav from './sticky-nav'
import hamburgerClick from './hamburger-click'
import menuClose from './mobile-menu-close'
import equalizerReinit from './equalizer-reinit'
import carousels from './lib/carousels'
import slideToggle from './slide-toggle'
//
import googleAnalytics from './google-analytics'
import googleMaps from './google-maps'
import darkStyle from './google-maps-styles/dark'
import greyStyle from './google-maps-styles/grey'
import hotjar from './hotjar'
import liveHelpNow from './live-help-now'

window.$ = $

import Foundation from 'foundation-sites'
// If you want to pick and choose which modules to include, comment out the above and uncomment
// the line below
//import './lib/foundation-explicit-pieces';

// ZURB Foundation
$(document).foundation()

//Carousels
carousels()

//Sticky Nav
//stickyNav()

//Hamburger Click
hamburgerClick()

//Menu Close
menuClose()

//Equalizer Reinit
equalizerReinit()

// Live Help Now
liveHelpNow()

// Google Analytics
googleAnalytics()

// Google Maps
const stylePicker = style => (style === 'grey' ? greyStyle : null)
googleMaps(stylePicker)

// Carousels
//carousels('#my-carousel')
