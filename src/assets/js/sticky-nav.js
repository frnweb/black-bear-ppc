import $ from 'jquery';
import debounce from 'lodash.debounce'

const scrollHandler = (event, triggerPoint, $navBar) => {
			const currentY = event.currentTarget.scrollY

			if (currentY >= triggerPoint) {
				$navBar.addClass('navbar-visible')

			} else {
				$navBar.removeClass('navbar-visible')
			}
		}

export default () => {

	$(document).ready(() => {
		const triggerPoint = $('#nav-triggerpoint').offset().top
		const $navBar = $('#responsive-menu')


		$(window).on("scroll", debounce((event) => {
			scrollHandler(event, triggerPoint, $navBar)
		}, 50))

	})
	
}