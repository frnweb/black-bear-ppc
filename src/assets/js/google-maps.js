import $ from 'jquery'
import googleMapsLoader from 'load-google-maps-api'
//

export const apiKey = 'AIzaSyAmjOz9vdCVJTyNqZMKTjTVelFH30oD8f0'

export const defaultMinHeight = 400
export const defaultHeight = '100%'

const ensureElementHasHeight = (
  $el,
  minHeight = defaultMinHeight,
  height = defaultHeight
) => {
  if ($el.height() <= 0) {
    $el.css({ minHeight, height })
  }
}

const readOptionsFromData = $el => {
  const lat = $el.data('map-lat')
  const lng = $el.data('map-lng')
  const zoom = $el.data('map-zoom')

  if (!lat) {
    throw new Error('Please add a "data-map-lat" attribute with the latitude.')
  } else if (!lng) {
    throw new Error('Please add a "data-map-lng" attribute with the longitude.')
  } else if (!zoom) {
    throw new Error(
      'Please add a "data-map-zoom" attribute with the map zoom level.'
    )
  }

  return {
    center: {
      lat,
      lng,
    },
    zoom,
  }
}

export const googleMaps = stylePredicate => {
  $(document).ready(() => {
    const loader = googleMapsLoader({ key: apiKey })

    $('[data-map]').each((index, el) => {
      const $el = $(el)

      let styles

      if (typeof stylePredicate === 'function') {
        styles = stylePredicate($el.data('map-style'))
      }

      ensureElementHasHeight($el)

      const options = Object.assign({}, readOptionsFromData($el), { styles })

      loader.then(google => {
        const map = new google.Map($el.get()[0], options)
        const marker = new google.Marker({ position: options.center, map, icon: 'assets/img/map-pin.svg'})

          const infoWindow = new google.InfoWindow({
            content: `
<div>
  <h4>
    Black Bear Lodge
  </h4>
  <p>
    310 Black Bear Ridge<br>Sautee Nacoochee, Georgia 30571
  </p>
  <a class="button primary" href="https://www.google.com/maps/dir/36.0360267,-86.8157976/Black+Bear+Lodge,+Sautee+Nacoochee,+GA/@35.2226714,-85.8296887,9z/data=!3m1!4b1!4m9!4m8!1m1!4e1!1m5!1m1!1s0x885f485504f17c83:0x124b3e62316d2e35!2m2!1d-83.7220909!2d34.6592352">Get Directions</a>
</div>
  `,
          })

          marker.addListener('click', () => {
            infoWindow.open(map, marker)
          })
      })
    })
  })
}

export default googleMaps
