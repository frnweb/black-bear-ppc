const loadlhn = () => {
  window.lhnJsSdkInit = function () {
    lhnJsSdk.setup = {
      application_id: "2cb55594-3a4a-4b89-bf4b-9f1a6cfdbff3",
      application_secret: "57d499aba56340a4a6916b981ad582f9b98f322ee04641a0bc"
    };
    lhnJsSdk.controls = [{
      type: "hoc",
      id: "d1b4a7f4-dcbb-40ee-9782-4d36fd313151"
    }];
    lhnJsSdk.dictionary = { 
          agentConnecting: "Connecting to agent",
          callbackMenu: "Callback",
          callbackTitle: "Request a callback",
          cancel: "Cancel",
          chatMenu: "Chat",
          chatTitle: "Conversation",
          email: "Email",
          endChat: "End Chat",
          endChatConfirm: "Are you sure you want to end the current chat?",
          inviteCancel: 'Dismiss',
          inviteStart: 'Chat now',
          knowledgeMenu: "FAQ",
          knowledgeTitle: "Search Knowledge",
          livechat: "LIVE CHAT",
          livechat_offline: "GET HELP",
          newChatTitle: "Chat With Us",
          offlineTitle: "Leave a message",
          send: "Send",
          startChat: "Start Chat",
          submit: "Submit",
          surveyTitle: "Survey",
          ticketMenu: "Ticket",
          ticketTitle: "Submit a ticket"
        };
  };

  (function (d, s) {
    var newjs, lhnjs = d.getElementsByTagName(s)[0];
    newjs = d.createElement(s);
    newjs.src = "https://developer.livehelpnow.net/js/sdk/lhn-jssdk-current.min.js";
    lhnjs.parentNode.insertBefore(newjs, lhnjs);
  }(document, "script"));
}



import $ from 'jquery'
import lhn from 'live-help-now'

export default () => {
  
  loadlhn()
  
  $(document).ready(() => {
    const $targets = $('[data-chat]')

    setInterval(() => {
      const onlineState = lhnJsSdk.isOnline
      if (typeof onlineState === 'undefined') {
        // script not loaded
        return
      }

      if (onlineState) {
        // chat is online
        $targets.each((index, item) => {
          const $item = $(item)
          const onlineText = $item.data('chat-online-text')

          $item.text(onlineText)//Live Chat Text
          $item.addClass('active')//Make Visible
        })
  
      } else {
        // chat is offline
        $targets.each((index, item) => {
          const $item = $(item)
          const onlineText = $item.data('chat-online-text')
          const offlineText = $item.data('chat-offline-text')
          const initialText = $item.text()

          $item.addClass('active')//Make Visible
          $item.text(offlineText) // set default text if no online text set
        })
      }
    }, 2000)  

    $targets.on('click', () => {
      lhnJsSdk.openHOC()
    })
  })
}
