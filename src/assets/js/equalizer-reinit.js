import $ from 'jquery';
import Foundation from 'foundation-sites'

export default () => {
$(document).ready(function() {
    var reInitCount = 0;
    window.setTimeout(function() {
        if (reInitCount < 6) {
            Foundation.reInit('equalizer');
        }
        
        reInitCount++;
    }, 2000);
});
}